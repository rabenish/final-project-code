//Database connection method

// Held constructor for the use in the file
const { Connection, Request } = require("tedious");


// These are the parameters that will be set for the request
const params = {
  authentication: {
    options: {
      userName: this.generateKeys.publicKey, //Use of the public key associated with the patients name
      password: this.getPrivateKeys.privateKey //Use of the private key used for the password
    },
    type: "default"
  },
  server: "Private_Medical_Server.net",
  options: {
    database: "Specific_Database",
    encrypt: true
  }
};




//the constrstor that will create the connection call
const newConnect = new Connection(request);

//the connection process takes place
newConnect.on("connect", error => { if(error) {
    console.error(error.message);}
    else {
        //if there is an error report to the 
        gatherData();
    }
});


//move on to actulally querring the database
function gatherData() {
    //create the request script            //selecting the data from the username or patient
    const request = new Request( 'SELECT pub_key, enc_data, hash_patient FROM [UserName]', 
    (error, rowCount) => {
        if (error) {
            console.log(err.message);
        }
        else {
            console.log('the patient records returned');
        }
    });
    newConnect.execSql(request);
}

//With the use of the 'crypto' library we are able to create assymetric keys for the use in the database request
class generateKeys {
    //This is where the implentation of a private and public key would be used to
    //give us access to a private key and a public key for the patient access

const { generateKeys } = require('crypto');

//Calls the generate keys method that will be used to define the keys for a patient
crypto.generateKeys('rsa', {
modulusLength: 600, 
publicKeyEncoding: {
	type: 'pkcs1',
	format: 'der'
},
privateKeyEncoding: {
	type: 'pkcs1',
	format: 'der',
	cipher: 'aes-256-cbc', 
}
});
console.log("Public Key is : ", publicKey.toString('hex'));
console.log("Private Key is: ", privateKey.toString('hex'));
}
